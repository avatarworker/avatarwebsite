import { AvatarWebSitePage } from './app.po';

describe('avatar-web-site App', () => {
  let page: AvatarWebSitePage;

  beforeEach(() => {
    page = new AvatarWebSitePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
