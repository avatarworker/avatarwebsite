import {Injectable} from "@angular/core";
declare const UnityLoader: any;
@Injectable()
export class Steveutil {
  private cht= 6;
  private radius= 12;
  public static gameInstance: any= null;


  steveInit(divName) {
    setTimeout(function(){
      Steveutil.gameInstance = UnityLoader.instantiate(divName, "../assets/steve/Avatar-0.12.json", {onProgress: UnityProgress, Module: {state: true, setStatus: setstate}});
    }, 0);
  }
  radiusOper(type){
    this.radius = type === "+" ? this.radius + 1 : this.radius - 1;
    Steveutil.gameInstance.SendMessage("Main Camera", "ResetCameraRadius", this.radius);
  }
  chtOper(type){
    this.cht = type === "+" ? this.cht + 1 : this.cht - 1;
    Steveutil.gameInstance.SendMessage("Main Camera", "ResetCameraHeight", this.cht);
  }
  resetSteve(){
    this.radius = 12;
    this.cht = 6;
    Steveutil.gameInstance.SendMessage("Main Camera", "ResetCameraRadius", this.radius);
    Steveutil.gameInstance.SendMessage("Main Camera", "ResetCameraHeight", this.cht);
  }
  clearSkins() {
    Steveutil.gameInstance.SendMessage("WebHandler", "ClearSkins");
  }
  unloadSkin(skinType, btn) {
    Steveutil.gameInstance.SendMessage("WebHandler", "UnloadSkin", skinType);
  }
  loadSkin(skinPath, skinType) {
    Steveutil.gameInstance.SendMessage("WebHandler", "LoadSkin", skinPath);
  }
  setBackgroundColor(color) {
    Steveutil.gameInstance.SendMessage("WebHandler", "SetBackgroundColor", color);
  }
  setBackgroundImg(imgPath) {
    Steveutil.gameInstance.SendMessage("WebHandler", "LoadBackground", "../assets/images/" + imgPath);
  }
}

function setstate(state){
  console.log(state);
}
function UnityProgress(gameInstance, progress) {
  if (!gameInstance.Module)
    return;
  if (!gameInstance.logo) {
    gameInstance.logo = document.createElement("div");
    gameInstance.logo.className = "logo " + gameInstance.Module.splashScreenStyle;
    // gameInstance.container.appendChild(gameInstance.logo);
  }
  if (!gameInstance.progress) {
    gameInstance.progress = document.createElement("div");
    gameInstance.progress.className = "progress " + gameInstance.Module.splashScreenStyle;
    gameInstance.progress.empty = document.createElement("div");
    gameInstance.progress.empty.className = "empty";
    gameInstance.progress.appendChild(gameInstance.progress.empty);
    gameInstance.progress.full = document.createElement("div");
    gameInstance.progress.full.className = "full";
    gameInstance.progress.appendChild(gameInstance.progress.full);
    gameInstance.container.appendChild(gameInstance.progress);
  }
  gameInstance.progress.full.style.width = (100 * progress) + "%";
  gameInstance.progress.empty.style.width = (100 * (1 - progress)) + "%";
  if (progress === 1)
    gameInstance.logo.style.display = gameInstance.progress.style.display = "none";
}
function onPreviewLoadFinish(e) {
  // alert("OnPreviewLoadFinish");
  console.log("onPreviewLoadFinish");
}
