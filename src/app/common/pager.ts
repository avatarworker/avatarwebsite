export class Pager<T> {

  /**每页多少条**/
  private _pageSize: number;
  /**当前页码**/
  private _currentPage: number;
  /**一共有多少数据**/
  private _records: number;
  /**分页数据**/
  private _value: Array<T>;
  constructor() {
    this._pageSize = 14;
    this._currentPage = 1;
    this._value = new Array();
  }


  get pageSize(): number {
    return this._pageSize;
  }

  get currentPage(): number {
    return this._currentPage;
  }


  set pageSize(value: number) {
    this._pageSize = value;
  }

  set currentPage(value: number) {
    this._currentPage = value;
  }

  get records(): number {
    return this._records;
  }

  get value(): Array<T> {
    return this._value;
  }
}
