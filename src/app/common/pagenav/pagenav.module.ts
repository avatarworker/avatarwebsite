import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagenavComponent } from './pagenav.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [PagenavComponent]
})
export class PagenavModule { }
