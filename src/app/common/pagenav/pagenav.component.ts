import {Component, Input, OnInit} from '@angular/core';
import {Pager} from "../pager";

@Component({
  selector: 'app-pagenav',
  templateUrl: './pagenav.component.html',
  styleUrls: ['./pagenav.component.css']
})
export class PagenavComponent implements OnInit {


  @Input() pager: Pager;
  constructor() { }

  ngOnInit() {
  }

}
