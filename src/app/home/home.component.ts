import { Component, OnInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
   this.initslider();
  }
  initslider(){
    const $sliders = $('[data-am-widget="slider"]');
    $sliders.not('.am-slider-manual').each(function(i, item) {
      // var options = UI.utils.parseOptions($(item).attr('data-am-slider'));
      const string = "data-am-slider";
      if ($.isPlainObject(string)) {
        return string;
      }
      const start = (string ? string.indexOf('{') : -1);
      let options = {};
      if (start != -1) {
        try {
          options = (new Function('',
            'var json = ' + string.substr(start) +
            '; return JSON.parse(JSON.stringify(json));'))();
        } catch (e) {
        }
      }
      console.log(options);
      $(item).flexslider(options);
    });
  }
}
