import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import { LoginDialogComponent } from './login-dialog.component';
import { MdDialogModule} from "@angular/material";

@NgModule({
  imports: [
    CommonModule,
    MdDialogModule,
    FormsModule
  ],
  declarations: [LoginDialogComponent]
})
export class LoginDialogModule { }
