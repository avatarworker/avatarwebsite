import { Component, OnInit } from '@angular/core';
import {MdDialogRef} from "@angular/material";
import {Router} from "@angular/router";
@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.css']
})
export class LoginDialogComponent implements OnInit {
  public userName= "";
  public userPwd= "";
  constructor(public dialogRef: MdDialogRef<LoginDialogComponent>, private router: Router, ) {

  }

  ngOnInit() {
  }
  login() {
    console.log(this.userName);
    console.log(this.userPwd);
    this.dialogRef.close(this.dialogRef);
    this.router.navigate(["usercenter"]);

  }
}
