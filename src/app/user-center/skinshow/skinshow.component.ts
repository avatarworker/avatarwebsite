import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-skinshow',
  templateUrl: './skinshow.component.html',
  styleUrls: ['./skinshow.component.css']
})
export class SkinshowComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    console.log("SkinshowComponent");
  }

}
