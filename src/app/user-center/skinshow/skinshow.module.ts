import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SkinshowComponent } from './skinshow.component';
import {RouterModule, Routes} from "@angular/router";
import {StevedisplayComponent} from "./stevedisplay/stevedisplay.component";
import {SkinlistComponent} from "./skinlist/skinlist.component";

const skinShowRouter: Routes = [
  {
    path: "",
    component: SkinshowComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(skinShowRouter)

  ],
  declarations: [
    SkinshowComponent,
    StevedisplayComponent,
    SkinlistComponent
  ]
})
export class SkinshowModule { }
