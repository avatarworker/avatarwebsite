import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkinshowComponent } from './skinshow.component';

describe('SkinshowComponent', () => {
  let component: SkinshowComponent;
  let fixture: ComponentFixture<SkinshowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkinshowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkinshowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
