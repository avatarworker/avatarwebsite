import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StevedisplayComponent } from './stevedisplay.component';

describe('StevedisplayComponent', () => {
  let component: StevedisplayComponent;
  let fixture: ComponentFixture<StevedisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StevedisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StevedisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
