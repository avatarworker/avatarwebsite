import { Component, OnInit } from '@angular/core';
import {Appconfig} from "../../../appconfig";
import {Steveutil} from "../../../common/steveutil";
@Component({
  selector: 'app-stevedisplay',
  templateUrl: './stevedisplay.component.html',
  styleUrls: ['./stevedisplay.component.css'],
  providers: []
})
export class StevedisplayComponent implements OnInit {
  public steveinstance;
  constructor(public steveutil: Steveutil) {
  }

  ngOnInit() {
     // gameInstance = UnityLoader.instantiate("gameContainer", "../../../assets/steve/Avatar-0.12.json", "gameModel");
     this.steveutil.steveInit("gameContainer");


  }


}

