import { Component, OnInit } from '@angular/core';
import {SkinService} from "../../../service/skin/skin.service";
import {Pager} from "../../../common/pager";
import {Appconfig} from "../../../appconfig";
import {Steveutil} from "../../../common/steveutil";
@Component({
  selector: 'app-skinlist',
  templateUrl: './skinlist.component.html',
  styleUrls: ['./skinlist.component.css'],
  providers: [SkinService]
})
export class SkinlistComponent implements OnInit {
  skinservice: SkinService;
  public pager: Pager= new Pager();
  constructor(skinservice: SkinService, public steveutil: Steveutil) {
    this.skinservice = skinservice;

  }

  ngOnInit() {
    this.skinservice.getSkinData(this.pager).subscribe(
      (data: Pager) => {
        this.pager = data;
      }
    );
  }
  public next(){
    this.pager.currentPage = this.pager.currentPage + 1;
    this.skinservice.getSkinData(this.pager).subscribe(
      (data: Pager) => {
        this.pager = data;
      }
    );
  }
  public pre(){
    this.pager.currentPage = this.pager.currentPage - 1;
    this.skinservice.getSkinData(this.pager).subscribe(
      (data: Pager) => {
        this.pager = data;

      }
    );
  }
  public shichuan(skin) {
    console.log(this.steveutil);
    const url = "/skinitem/downloadSkinitem/" + skin.id + ".do";
    this.steveutil.loadSkin(Appconfig.comitUrl(url), "1");

  }

}
