import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkinlistComponent } from './skinlist.component';

describe('SkinlistComponent', () => {
  let component: SkinlistComponent;
  let fixture: ComponentFixture<SkinlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkinlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkinlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
