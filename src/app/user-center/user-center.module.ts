import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UserCenterComponent} from "./user-center.component";
import {RouterModule, Routes} from "@angular/router";
import {MaterialModule, MdNativeDateModule} from "@angular/material";

const userCenterRouter: Routes = [
  {
    path: "",
    component: UserCenterComponent,
    children: [
      {
        path: "",
        loadChildren: 'app/user-center/skinshow/skinshow.module#SkinshowModule'
      },
      {
        path: "**",
        loadChildren: 'app/user-center/skinshow/skinshow.module#SkinshowModule'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    MdNativeDateModule,
    RouterModule.forChild(userCenterRouter)
  ],
  declarations: [
    UserCenterComponent
  ]
})
export class UserCenterModule { }
