import { Component } from '@angular/core';
import {MdDialog} from "@angular/material";
import {LoginDialogComponent} from "./login-dialog/login-dialog.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(public dialog: MdDialog) {

  }
  openDialog() {
    const dialogRef = this.dialog.open(LoginDialogComponent, {
      height: '450px',
      width: '600px'
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }
}
