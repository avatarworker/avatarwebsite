import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LoginDialogModule} from './login-dialog/login-dialog.module';
import {LoginDialogComponent} from './login-dialog/login-dialog.component';
import {RouterModule, Routes} from "@angular/router";
import {Steveutil} from "./common/steveutil";
const appRoutes: Routes = [
  {
    path: "",
    loadChildren: 'app/home/home.module#HomeModule'
  },
  {
    path: "about",
    loadChildren: 'app/aboutus/aboutus.module#AboutusModule'
  },
  {
    path: "usercenter",
    loadChildren: 'app/user-center/user-center.module#UserCenterModule'
  },
  {
    path: "**",
    loadChildren: 'app/home/home.module#HomeModule'
  }

];
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    LoginDialogModule,
    RouterModule.forRoot(appRoutes)
  ],
  entryComponents: [
    LoginDialogComponent
  ],
  providers: [Steveutil],
  bootstrap: [AppComponent]
})
export class AppModule { }
