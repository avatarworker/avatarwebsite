import { TestBed, inject } from '@angular/core/testing';

import { SkinService } from './skin.service';

describe('SkinService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SkinService]
    });
  });

  it('should be created', inject([SkinService], (service: SkinService) => {
    expect(service).toBeTruthy();
  }));
});
