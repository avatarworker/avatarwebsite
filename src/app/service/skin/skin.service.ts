import {Component, Injectable} from '@angular/core';
import {Http} from "@angular/http";
import {Appconfig} from "../../appconfig";
import {Pager} from "../../common/pager";
import {Observable} from "rxjs";
import 'rxjs/Rx';
@Injectable()
export class SkinService {
  constructor(private http: Http) {
    this.http = http;
  }
 public getSkinData(pagerdata: Pager): Observable<Pager> {
   return this.http.post(Appconfig.comitUrl("/skinitem/list/page.do"), {pager: pagerdata }).map((res: any) => {
       let pager: Pager;
       pager = (<Pager>res.json());

       return pager;
    });
 }


}
