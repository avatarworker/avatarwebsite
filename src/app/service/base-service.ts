import {Pager} from "../common/pager";
export interface BaseService<T> {
  public getPageData(): Pager;
}
