import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AboutusComponent} from "./aboutus.component";
import {RouterModule, Routes} from "@angular/router";
const aboutRouters: Routes = [
  {
    path: "",
    component: AboutusModule
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(aboutRouters)
  ],
  declarations: [
    AboutusComponent
  ]
})
export class AboutusModule { }
