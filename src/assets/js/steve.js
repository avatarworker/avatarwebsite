Steve={
  cht:6,
  radius:12,
  gameInstance:null,
  steveInit:function(divname){
    setTimeout(function(){
      this.gameInstance = UnityLoader.instantiate(divname, "../assets/steve/Avatar-0.12.json",{onProgress: UnityProgress,Module:{state:true,setStatus:setstate}});
    },0);

   return this;
  },
  radiusOper:function(type){
    this.radius=type==="+"?this.radius+1:this.radius-1;
    this.gameInstance.SendMessage("Main Camera", "ResetCameraRadius", this.radius);
  },
  chtOper:function(type){
    this.cht=type==="+"?this.cht+1:this.cht-1;
    this.gameInstance.SendMessage("Main Camera", "ResetCameraHeight", this.cht);
  },
  resetSteve:function(){
    this.radius=12;
    this.cht=6;
    this.gameInstance.SendMessage("Main Camera", "ResetCameraRadius", this.radius);
    this.gameInstance.SendMessage("Main Camera", "ResetCameraHeight", this.cht);
  },
  clearSkins:function(){
    this.gameInstance.SendMessage("WebHandler", "ClearSkins");
  },
  unloadSkin:function(skinType,btn){
    this.gameInstance.SendMessage("WebHandler", "UnloadSkin", skinType);
  },
  loadSkin:function(skinPath,skinType){
    debugger;
    this.gameInstance.SendMessage("WebHandler", "LoadSkin", skinPath);
  },
  setBackgroundColor:function(color){
    this.gameInstance.SendMessage("WebHandler", "SetBackgroundColor", color);
  },
  setBackgroundImg:function(imgPath){
    this.gameInstance.SendMessage("WebHandler", "LoadBackground", "../assets/images/"+imgPath);
  }


};
function setstate(state){
  console.log(state);
}
function UnityProgress(gameInstance, progress) {
  if (!gameInstance.Module)
    return;
  if (!gameInstance.logo) {
    gameInstance.logo = document.createElement("div");
    gameInstance.logo.className = "logo " + gameInstance.Module.splashScreenStyle;
    // gameInstance.container.appendChild(gameInstance.logo);
  }
  if (!gameInstance.progress) {
    gameInstance.progress = document.createElement("div");
    gameInstance.progress.className = "progress " + gameInstance.Module.splashScreenStyle;
    gameInstance.progress.empty = document.createElement("div");
    gameInstance.progress.empty.className = "empty";
    gameInstance.progress.appendChild(gameInstance.progress.empty);
    gameInstance.progress.full = document.createElement("div");
    gameInstance.progress.full.className = "full";
    gameInstance.progress.appendChild(gameInstance.progress.full);
    gameInstance.container.appendChild(gameInstance.progress);
  }
  gameInstance.progress.full.style.width = (100 * progress) + "%";
  gameInstance.progress.empty.style.width = (100 * (1 - progress)) + "%";
  if (progress == 1)
    gameInstance.logo.style.display = gameInstance.progress.style.display = "none";
}
function onPreviewLoadFinish(e) {
  // alert("OnPreviewLoadFinish");
  console.log("onPreviewLoadFinish");
}
